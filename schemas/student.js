const mongoose = require('mongoose');
const Schema = mongoose.Schema;

/* Описание полей схемы профиля
 *
 */

const studentSchema = new Schema({
    user: { type: String, required: true, unique: true },
    password: { type: String, required: true, unique: false },
    name: { type: String, deafult: '' },
    superuser: { type: Boolean, required: false, default: false },
    photo: { type: String, deafult: '' },
    description: { type: String, deafult: '' },
    about: { type: String, deafult: '' },
    links: [{
        name: { type: String },
        link: { type: String }
    }],
    skills: [{
        name: { type: String },
        description: { type: String },
        imageLink: { type: String }
    }]
});

const Student = mongoose.model('Student', studentSchema);

// db.students.insert({ user: 'nmozharov', password: '55555', superuser: true })

module.exports = Student;