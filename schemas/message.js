const mongoose = require('mongoose');
const Schema = mongoose.Schema;

/* Описание полей схемы сообщений
 * user - имя пользователя
 * text - текст сообщения
 * date - дата и время сообщения
 *
 * */

const messageSchema = new Schema({
    user: { type: String, required: true, unique: false },
    image: { type: String, required: false, unique: false },
    text: { type: String, required: true },
    date: { type: Date },
});

const Message = mongoose.model('Message', messageSchema);

module.exports = Message;