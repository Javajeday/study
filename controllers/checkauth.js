const Student = require('../schemas/student');

const controller = (req, res) => {
    if (!req.session.user) {
        res.end(403);
    }

    Student.findOne({ '_id': req.session.user }, (err, student) => {
        if (err) throw new Error(err);
        if (student) {
            req.session.user = student._id;
            req.session.password = student.password;

            res.json({
                user: student.user,
                name: student.name,
                superuser: student.superuser,
                photo: student.photo,
                description: student.description,
                about: student.about,
                links: student.links,
                skills: student.skills
            });
        } else {
            res.json({ error: 'Вы не авторизованы' })
        }
    });
};

module.exports = controller;