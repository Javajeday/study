const Message = require('../schemas/message.js');
const async = require('async');

const controller = (req, res) => {
    async.waterfall([
        callback => {
            if (req.body && req.body.user && req.body.text) {
                let newMessage = JSON.parse(JSON.stringify(req.body));
                newMessage.date = Date.now();
                Message.create(req.body, (err, message) => {
                    if (err) throw new Error(err);
                    callback();
                });
            } else {
                callback();
            }
        },
        callback => {
            Message.find()
                .sort({date: 'desc'})
                .limit(50)
                .exec((err, messages) => {
                    if (err) throw new Error(err);
                    if (!messages) {
                        callback(null, false);
                    } else if (messages) {
                        callback(null, messages)
                    } else {
                        res.json({errorMessage: 'Ошибка сервера. Перезагрузите страницу'})
                    }
                });
        }
    ], (err, messages) => {
        if (err) {
            throw new Error(err)
        }

        if (messages) {
            res.json(messages.reverse());
        } else {
            res.sendStatus(403);
        }
    });
};

module.exports = controller;