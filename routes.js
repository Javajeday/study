const message = require('./controllers/message');
const messages = require('./controllers/messages');
const redirect = require('./controllers/redirect');
const simple = require('./controllers/simple');
const login = require('./controllers/login');
const checkauth = require('./controllers/checkauth');
const edit = require('./controllers/edit');
const logout = require('./controllers/logout');

const routes = {
    message: {
        method: 'POST',
        url: '/messages',
        controller: message
    },
    messages: {
        method: 'GET',
        url: '/messages',
        controller: messages
    },
    redirect: {
        method: 'GET',
        url: '/redirect',
        controller: redirect
    },
    simple: {
        method: 'GET',
        url: '/simple',
        controller: simple
    },
    redirectPost: {
        method: 'POST',
        url: '/redirect',
        controller: redirect
    },
    simplePost: {
        method: 'POST',
        url: '/simple',
        controller: simple
    },
    login: {
        method: 'POST',
        url: '/auth/login',
        controller: login
    },
    edit: {
        method: 'POST',
        url: '/edit',
        controller: edit
    },
    cheackAuth: {
        method: 'POST',
        url: '/checkauth',
        controller: checkauth
    },
    logout: {
        method: 'POST',
        url: '/logout',
        controller: logout
    }
};

module.exports = routes;