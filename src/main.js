import Vue from 'vue'
import App from './App'
import router from './router/index'
import store from './store'
import auth from './auth/auth'
import axios from 'axios'
import VueAxios from 'vue-axios'
import miniToastr from 'mini-toastr'
import VueNotifications from 'vue-notifications'

const defaultBackEnd = 'http://frontend-mai.ru';
const overrideBackEnd = process.env.API;

window.API = overrideBackEnd || 'http://frontend-mai.ru';

const toastTypes = {
    success: 'success',
    error: 'error',
    info: 'info',
    warn: 'warn'
};

miniToastr.init({ types: toastTypes });
function toast({ title, message, type, timeout, cb }) {
    return miniToastr[type](message, title, timeout, cb)
}

const options = {
    success: toast,
    error: toast,
    info: toast,
    warn: toast
};

Vue.use(VueNotifications, options);
Vue.config.productionTip = false;
Vue.use(VueAxios, axios);

router.beforeEach((to, from, next) => {
    console.info(`Transition from ${from.name} to ${to.name}`);
    if (to.name === 'edit') {
        axios.post(`${window.API}/checkauth`).then(response => {
            if (response.data.error) {
                next({
                    path: '/login'
                })
            } else {
                store.commit('setUserData', response.data)
                next();
            }
        }, error => {
            next({
                path: '/login'
            })
        })
    } else {
        next()
    }
});

/* eslint-disable no-new */
new Vue({
    el: '#app',
    store,
    router,
    components: { App },
    template: '<App/>'
});
