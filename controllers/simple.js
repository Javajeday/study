const controller = (req, res) => {
    console.log(req.query);
    console.log(req.body);
    if (req.body && Object.keys(req.body).length) {
        if (req.body.pow && req.body.value) {
            res.json({ value: Math.pow(req.body.value, 2) })
        } else if (req.body.getjson) {
            res.json({ a: 1, someProp: 'Simple string', someData: [{porp1: 1}, {prop2: 2}] });
        } else if (req.body.gettext) {
            res.send('Этот текст вы получили сделав POST запрос c gettext');
        } else {
            res.sendStatus(200)
        }
    } else if (req.query && Object.keys(req.query).length) {
        if (req.query.reverseword) {
            res.json(req.query.reverseword.split('').reverse().join(''));
        } else if (req.query.givemejson) {
            res.json({ a: 1, someProp: 'Simple string', someData: [{porp1: 1}, {prop2: 2}] });
        } else if (req.query.givemetext) {
            res.send('Этот текст вы получили сделав GET запрос');
        } else if (req.query.givemeerror) {
            res.sendStatus(500);
        }
    } else {
        res.sendStatus(200)
    }
};

module.exports = controller;