const messageContainer = document.querySelector('.messages');
const userNameNode = document.querySelector('.username');
const userTextNode = document.querySelector('.text-message');
const sendButton = document.querySelector('button');
let messages = [];

function getMessages() {
    let options = {
        method: 'GET',
        headers: {
            'Accept': 'application/json'
        }
    }
    fetch('/messages', options)
        .then(res => { return res.json() })
        .then(res => {
            messages = res;
            renderMessages();
        })
}

function sendMessage(user, text) {
    let data = JSON.stringify({ user: user, text: text });
    let options = {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-type': 'application/json'
        },
        body: data
    }
    fetch('/messages', options)
        .then(res => { return res.json() })
        .then(res => {
            messages = res;
            renderMessages();
            userTextNode.value = '';
        })
}

function renderMessages() {
    messageContainer.innerHTML = '';

    messages.reverse().forEach(message => {
        const userName = message.user + ':';
        const text = ' ' + message.text;
        const messageRoot = document.createElement('div');
        const userNameNode = document.createElement('span');
        const textNode = document.createElement('span');

        userNameNode.innerText = userName;
        textNode.innerText = text;

        messageRoot.classList.add('message');
        userNameNode.classList.add('user');
        textNode.classList.add('text');

        messageRoot.appendChild(userNameNode);
        messageRoot.appendChild(textNode);
        messageContainer.appendChild(messageRoot);
    });

    messageContainer.scroll({
        top: 9999,
        left: 0,
        behavior: 'smooth'
    })

}

setInterval(getMessages, 3000);

sendButton.addEventListener('click', () => {
    sendMessage(userNameNode.value, userTextNode.value);
});