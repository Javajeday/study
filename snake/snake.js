class SnakeGame {
    constructor(pgDimension, speed) {
        this.snake = [];
        this.food = null;
        this.game = null;
        this.direction = 'left';
        this.speed = speed;
        this.dimension = pgDimension;
        this.rootNode = document.querySelector('.snake-game');
        this.playGround = this.rootNode.querySelector('.playground');
        this.startButton = this.rootNode.querySelector('.start-button');
        this.renderPlayGround();
        this.attachControlEvents();
    }

    renderPlayGround() {
        for (let i = 0; i < Math.pow(this.dimension, 2); i++) {
            let square = document.createElement('div');
            let row = Math.floor(i / this.dimension);
            let column = i - (row * this.dimension);
            square.classList.add('square');
            square.setAttribute('row', row);
            square.setAttribute('column', column);
            this.playGround.appendChild(square);
        }
    }

    attachControlEvents() {
        window.addEventListener('keydown', (event) => {
            if (event.keyCode === 37) {
                this.direction = 'left'
            } else if (event.keyCode === 39) {
                this.direction = 'right'
            } else if (event.keyCode === 38) {
                this.direction = 'up'
            } else if (event.keyCode === 40) {
                this.direction = 'down'
            } else {}
        });

        this.startButton.addEventListener('click', () => {
            this.start();
        });
    }

    renderSnake() {
        var snakeElements = document.querySelectorAll('.head, .tail');
        snakeElements.forEach(el => {
            el.classList.remove('head');
            el.classList.remove('tail');
        })

        this.snake.forEach((part, index) => {
            if (index === 0) {
                let head = document.querySelector(`[row="${part.row}"][column="${part.column}"]`);
                head.classList.add('head');
            } else {
                let snakeTail = document.querySelector(`[row="${part.row}"][column="${part.column}"]`);
                snakeTail.classList.add('tail');
            }
        })
    }

    checkLoose() {
        let head = this.snake[0];
        this.snake.forEach((part, index) => {
            if (index > 1) {
                if (part.row === head.row && part.column === head.column) {
                    alert('U loose rofl');
                    clearInterval(this.game);
                }
            }
        })
    }

    start() {
        clearInterval(this.game);
        this.snake = [];
        this.game = null;
        this.food = null;
        this.initSnake();
        this.renderSnake();
        this.generateFood();

        this.game = setInterval(() => {
            this.snake = this.snake.map((part, index) => {
                if (index === 0) {
                    let newPosition = {
                        row: part.row,
                        column: part.column
                    }
                    if (this.direction === 'left') {
                        newPosition.column -= 1;
                    } else if (this.direction === 'right') {
                        newPosition.column += 1;
                    } else if (this.direction === 'up') {
                        newPosition.row -= 1;
                    } else if (this.direction === 'down') {
                        newPosition.row += 1;
                    }

                    if (newPosition.row > this.dimension - 1) {
                        newPosition.row = 0;
                    } else if (newPosition.row < 0) {
                        newPosition.row = this.dimension - 1;
                    }

                    if (newPosition.column > this.dimension - 1) {
                        newPosition.column = 0;
                    } else if (newPosition.column < 0) {
                        newPosition.column = this.dimension - 1;
                    }

                    return newPosition;
                } else {
                    return this.snake[index - 1];
                }
            })
            if (this.food.row === this.snake[0].row && this.food.column === this.snake[0].column) {
                this.snake.push(this.snake[this.snake.length - 1]);
                this.generateFood();
            }
            this.checkLoose();
            this.renderSnake();
        }, this.speed)
    }

    generateFood() {
        let currentFood = document.querySelector('.food');
        if (currentFood) {
            currentFood.classList.remove('food');
        }
        this.food = null;
        this.food = {
            row: Math.floor(Math.random() * this.dimension),
            column: Math.floor(Math.random() * this.dimension)
        }
        this.checkFoodCollision();
        let food = document.querySelector(`[row="${this.food.row}"][column="${this.food.column}"]`);
        food.classList.add('food');
    }

    checkFoodCollision() {
        this.snake.forEach(part => {
            if (part.row === this.food.row && part.column === this.food.column) {
                console.log('collision');
                this.generateFood();
            }
        })
    }

    initSnake() {
        this.snake.push({
            row: Math.floor(this.dimension / 2),
            column: Math.floor(this.dimension / 2)
        })
    }
}

let snakeGame = new SnakeGame(10, 250);