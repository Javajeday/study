const express = require('express');
const http = require('http');
const args = require('yargs').argv;
const colors = require('colors');
const cluster = require('cluster');
const numCPUs = require('os').cpus().length;
const async = require('async');
const busboy = require('connect-busboy');
const multer = require('multer');
const upload = multer({ dest: 'img' });
const cookieParser = require('cookie-parser');
const session = require('express-session');
const cors = require('cors')

const body = require('body-parser');
const mongodb = require('mongodb');
const mongoose = require('./dbconfig/dbconfig');
const Message = require('./schemas/message');
const MongoStore = require('connect-mongo')(session);
const attachStudentRoutes = require('./controllers/student');

const compression = require('compression');

const routes = require('./routes');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, 'img')
    },
    filename: (req, file, cb) => {
        cb(null, Date.now() + '-' + file.originalname)
    }
});

colors.enabled = true;
colors.setTheme({
    silly: 'rainbow',
    input: 'grey',
    verbose: 'cyan',
    prompt: 'grey',
    info: 'green',
    data: 'grey',
    help: 'cyan',
    warn: 'red',
    debug: 'blue',
    error: 'red'
});


const app = express();
app.use(body.json());
app.use(busboy());
app.use(express.static('./dist'));
app.use(express.static('./snake'));
app.use(express.static('./messenger'));
app.use(express.static('./img'));
app.use(express.static('./assets'));
app.use(cookieParser('hds6kjfhr3yw'));
app.use(compression());
app.use(cors({
    origin: 'http://localhost:8080',
    credentials: true
}));
app.use(session({
    resave: true,
    saveUninitialized: true,
    store: new MongoStore({ mongooseConnection: mongoose.connection }),
    secret: 'hds6kjfhr3yw',
    cookie: { maxAge: 86400000 }
}));
app.use(checkContentLength);
app.use(multer({
    storage: storage,
    limits: { fileSize: 5000000 }
}).fields([{ name: 'file0' }]));

/* Dedicated routes */
app.get('/*', (req, res) => {
    res.sendFile('index.html', { root: `${__dirname}/dist/` });
});

attachStudentRoutes(app);

app.get('/snake', (req, res) => {
    res.sendFile('snake.html', { root: `${__dirname}/snake/` });
});

app.get('/messenger', (req, res) => {
    res.sendFile('messenger.html', { root: `${__dirname}/messenger/` });
});
/* Dedicated routes */

/* Complex routes */
for (route in routes) {
    if (routes[route].method == 'GET') {
        app.get(routes[route].url, routes[route].controller)
    }
    if (routes[route].method == 'POST') {
        app.post(routes[route].url, routes[route].controller)
    }
}
/* Complex routes */

/* Check Content-Length for multer (it's internal checking is some buggy) */
function checkContentLength(req, res, next) {
    if (req.headers['content-length'] > 5000000) {
        res.send({ error: 'Большой файл' })
    } else {
        next();
    }
}
/* End Check Content-Length for multer */

if (cluster.isMaster) {
    for (let i = 0; i < numCPUs; i++) {
        cluster.fork();
    }
    cluster.on('online', (worker, code, signal) => {
        console.log((`Worker died`).error);
    });
    cluster.on('exit', (worker, code, signal) => {
        cluster.fork();
    });
} else {
    http.createServer(app).listen(args['port'] || 3000, () => {
        console.log((`Server running on port: ${args['port'] || 3000}`).silly);
    });
}
