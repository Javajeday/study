const Message = require('../schemas/message.js');
const async = require('async');

const controller = (req, res) => {
    async.waterfall([
        callback => {
            Message.find()
                .sort({date: 'desc'})
                .limit(50)
                .exec((err, messages) => {
                    if (err) throw new Error(err);
                    if (!messages) {
                        callback(null, false);
                    } else if (messages) {
                        callback(null, messages)
                    } else {
                        res.json({errorMessage: 'Ошибка сервера. Перезагрузите страницу'})
                    }
                });
        }
    ], (err, messages) => {
        if (err) {
            throw new Error(err)
        }

        if (messages) {
            res.json(messages.reverse());
        } else {
            res.sendStatus(403);
        }
    });
};

module.exports = controller;