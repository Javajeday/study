const Student = require('../schemas/student');

const controller = (req, res) => {
    let trimmedStudentName = req.body.user.trim().toLowerCase();

    Student.findOne({ 'user': trimmedStudentName }, (err, student) => {
        if (err) throw new Error(err);
        if (student && req.body && student.password === req.body.password) {
            req.session.user = student._id;
            req.session.password = student.password;

            res.json({
                user: student.user,
                name: student.name,
                superuser: student.superuser,
                photo: student.photo,
                description: student.description,
                links: student.links,
                skills: student.skills
            });
        } else {
            res.json({ error: 'Неправильный логин или пароль' })
        }
    });
};

module.exports = controller;