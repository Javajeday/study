const Student = require('../schemas/student');
const async = require('async');

const controller = (req, res) => {
    if (!req.session.user || !req.body) {
        res.sendStatus(403);
        return;
    }

    let skills = req.body.skills ? JSON.parse(req.body.skills) : [];
    let links = req.body.links ? JSON.parse(req.body.links) : [];

    const updatedStudent = {
        name: req.body.name,
        description: req.body.description,
        about: req.body.about,
        $set: { links: links, skills: skills }
    };

    if (req.files && req.files.file0) {
        let file = req.files.file0[0];
        if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
            Object.assign(updatedStudent, { photo: req.files.file0[0].filename })
        } else {
            res.send({ error: 'Проверьте файл. Max 5 MB. Jpeg/Png' });
            return;
        }
    }

    if (req.body.oldPassword && req.body.newPassword && req.body.newPassword.length) {
        if (req.body.oldPassword === req.session.password) {
            Object.assign(updatedStudent, { password: req.body.newPassword });
        } else {
            res.send({ error: 'Не правильный старый пароль' });
            return;
        }
    }

    Student.findOneAndUpdate({ '_id': req.session.user }, updatedStudent, (err, student) => {
        if (err) throw new Error(err);
        res.json(student);
    });

};

module.exports = controller;