const controller = (req, res) => {
    res.sendStatus(200);
    req.session.destroy();
};

module.exports = controller;