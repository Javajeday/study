import Vue from 'vue'
import Router from 'vue-router'
import main from '@/components/main'
import login from '@/components/login'
import pkostikov from '@/components/students/pkostikov/pkostikov'
import nmozharov from '@/components/students/nmozharov/nmozharov'
import mdzhimbeev from '@/components/students/mdzhimbeev/mdzhimbeev'
import tsabirzyanov from '@/components/students/tsabirzyanov/tsabirzyanov'
import Greg from '@/components/students/Greg/Greg'
import slopatkina from '@/components/students/slopatkina/slopatkina'
import vromashina from '@/components/students/vromashina/vromashina'
import evinogradova from '@/components/students/evinogradova/evinogradova'
import vlitvinov from '@/components/students/vlitvinov/vlitvinov'
import edit from '@/components/edit'
import students from '@/components/students/students'
import mzhukov from '@/components/students/mzhukov/mzhukov'
import sharmanov from '@/components/students/sharmanov/sharmanov'

Vue.use(Router);

export default new Router({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'mainpage',
            beforeRouteEnter (to, from, next) {
                console.log(to)
            },
            component: main
        },
        {
            path: '/login',
            name: 'login',
            component: login
        },
        {
            path: '/students',
            name: 'students',
            component: students
        },
        {
            path: '/nmozharov',
            name: 'nmozharov',
            component: nmozharov
        },
      {
        path: '/pkostikov',
        name: 'pkostikov',
        component: pkostikov
      },
        {
            path: '/vromashina',
            name: 'vromashina',
            component: vromashina
        },
        {
            path: '/edit',
            name: 'edit',
            component: edit
        },
        {

            path: '/gchernyshev',
            name: 'gchernyshev',
            component: Greg
        },
        {
            path: '/evinogradova',
            name: 'evinogradova',
            component: evinogradova
        },
        {
          path: '/mdzhimbeev',
          name: 'mdzhimbeev',
          component: mdzhimbeev
        },
        {
            path: '/tsabirzyanov',
            name: 'tsabirzyanov',
            component: tsabirzyanov
        },
        {
            path: '/mzhukov',
            name: 'mzhukov',
            component: mzhukov
        },
        {
            path: '/slopatkina',
            name: 'slopatkina',
            component: slopatkina
        },
        {
          path: '/vlitvinov',
          name: 'vlitvinov',
          component: vlitvinov
        },
        {
          path: '/sharmanov',
          name: 'sharmanov',
          component: sharmanov
        }
    ]
})
