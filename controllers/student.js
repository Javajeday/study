const Student = require('../schemas/student');

const studentsArray = [
    'nmozharov',
    'vromashina',
    'tsabirzyanov',
    'gchernyshev',
    'evinogradova',
    'mdzhimbeev',
    'vlitvinov',
    'mzhukov',
    'pkostikov',
    'slopatkina',
    'sharmanov',
    {
        url: '/students',
    }
];

attachStudentRoutes = (app) => {
    studentsArray.forEach(student => {
        if (typeof student === 'string') {
            console.log(`Add route: /${student}`);
            app.post(`/${student}`, (req, res) => {
                Student.findOne({ 'user': student }, (err, student) => {
                    if (err) throw new Error(err);
                    if (student) {
                        res.json({
                            about: student.about,
                            user: student.user,
                            name: student.name,
                            superuser: student.superuser,
                            photo: student.photo,
                            description: student.description,
                            links: student.links,
                            skills: student.skills
                        });
                    } else {
                        res.json({ error: 'Нет такого студента' })
                    }
                });
            })
        } else if (typeof student === 'object') {
            console.log(`Add route: ${student.url}`);
            app.post(`${student.url}`, (req, res) => {
                Student.find({}, (err, students) => {
                    if (err) throw new Error(err);
                    if (students && students.length) {
                        let distilled = [];
                        students.forEach(student => {
                            distilled.push({
                                user: student.user,
                                name: student.name,
                                superuser: student.superuser,
                                about: student.about,
                                photo: student.photo,
                                description: student.description,
                                links: student.links,
                                skills: student.skills
                            })
                        })
                        res.json(distilled);
                    } else {
                        res.json({ error: 'Нет студентов' })
                    }
                });
            })
        }
    })
}


module.exports = attachStudentRoutes;