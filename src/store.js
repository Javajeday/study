import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

const store = new Vuex.Store({
    state: {
        userData: {},
        currentStudent: {},
        students: []
    },
    getters: {
        getUserData: state => {
            return JSON.parse(JSON.stringify(state.userData))
        },
        getCurrentStudent: state => {
            return JSON.parse(JSON.stringify(state.currentStudent))
        },
        getStudents: state => {
            return JSON.parse(JSON.stringify(state.students))
        }
    },
    mutations: {
        logout (state) {
            state.userData = {};
        },
        setUserData (state, data) {
            state.userData = data
        },
        setStudentData (state, data) {
            state.currentStudent = data
        },
        setStudents (state, data) {
            console.log(data)
            state.students = data
        }
    }
});

export default store
