import router from '../router/index'
import Vue from 'vue'
import axios from 'axios'
import store from '../store'

axios.defaults.withCredentials = true;

export default {
    user: {
        authenticated: false
    },
    login(context, creds) {
        return Vue.axios.post(`${window.API}/auth/login`, creds);
    },
    logout() {
        return Vue.axios.post(`${window.API}/auth/logout`);
        router.push('/login');
    },
    checkAuth() {
        axios.post(`${window.API}/checkauth`).then(response => {
            if (response.data.error) {
                router.push({
                    path: '/login'
                })
            } else {
                store.commit('setUserData', response.data)
            }
        }, error => {
            router.push({
                path: '/login'
            })
        })
    }
}
