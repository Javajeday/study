const controller = (req, res) => {
    console.log(req.query);
    console.log(req.params);

    if (req.query && Object.keys(req.query).length) {
        if (req.query.link) {
            res.status(301).redirect(req.query.link);
        } else if (req.query.reverseword) {
            res.json(req.query.reverseword.split('').reverse().join());
        }
    }
    res.status(301).redirect('https://yandex.ru');
};

module.exports = controller;


